package com.automation.practice;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Amway {

	public static void main(String[] args) throws InterruptedException {

		String homePage = "https://www.uat.amway.in/";
		// String homePage = "https://www.youtube.com/";
		String url = "";
		HttpURLConnection huc = null;
		int respCode = 200;

		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();

		// driver.get("https://www.youtube.com/");
		driver.get("https://www.uat.amway.in/");
		Thread.sleep(5000l);
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);

		List<WebElement> links = driver.findElements(By.tagName("a"));
		links.addAll(driver.findElements(By.tagName("img")));

		System.out.println(links.size());
		Iterator<WebElement> it = links.iterator();
		int count = 0;
		int nullAnchorTags = 0;
		while (it.hasNext()) {

			url = it.next().getAttribute("href");

			// System.out.println(url);

			if (url == null || url.isEmpty()) {
				System.out.println("broker anchor link count-------"+ url+"   ----" +nullAnchorTags++);
				System.out.println("URL is either not configured for anchor tag or it is empty");
				continue;
			}

			if (!url.startsWith(homePage)) {
				System.out.println(url);
				System.out.println("not from amway-------"+nullAnchorTags++);
				System.out.println("URL belongs to another domain, skipping it.");
				continue;
			}

			try {
				huc = (HttpURLConnection) (new URL(url).openConnection());
				huc.setRequestMethod("HEAD");
				huc.connect();
				respCode = huc.getResponseCode();
				if (respCode >= 400) {
					System.out.println("broker link count --------"+count++);
					System.out.println(url + " is a broken link" + "respcode : " + respCode);
				} else {
					System.out.println(url + " is a valid link" + "respcode : " + respCode);
				}

			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
		System.out.println("total broken link in the page   :  " + count);
		System.out.println("total null or blank achor tags the page   :  " + nullAnchorTags);

		driver.quit();

	}

}
